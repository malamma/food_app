﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace food_app
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        bool menuUtama, isAyamGoreng, isNasiGoreng, isSoto;
        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                //MessageBox.Show("check 1");
                menuUtama = false;
                //isAyamGoreng = true;
                //isNasiGoreng = false;
                //isSoto = false;
                QuantityForm qf = new QuantityForm(radioButton1.Text, menuUtama, isAyamGoreng, isNasiGoreng, isSoto);
                qf.Show();
            }
            else if (radioButton2.Checked)
            {
                menuUtama = true;
                isAyamGoreng = false;
                isNasiGoreng = true;
                isSoto = false;
                QuantityForm qf = new QuantityForm(radioButton2.Text, menuUtama, isAyamGoreng, isNasiGoreng, isSoto);
                qf.Show();
            }
            else if (radioButton3.Checked)
            {
                menuUtama = true;
                isAyamGoreng = false;
                isNasiGoreng = false;
                isSoto = true;
                QuantityForm qf = new QuantityForm(radioButton3.Text, menuUtama, isAyamGoreng, isNasiGoreng, isSoto);
                qf.Show();
            }
        }
    }
}
