﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace food_app
{
    public partial class QuantityForm : Form
    {
        public bool isAyamGoreng, isNasiGoreng, isSoto;

        private bool menuTmbhn = false;
        private string text, txt;

        Makanan mk = new Makanan();
        AyamGoreng ag = new AyamGoreng();
        NasiGoreng ng = new NasiGoreng();
        SotoAyam sa = new SotoAyam();

        public QuantityForm(string text1, bool menu, bool isAyamGoreng, bool isNasiGoreng, bool isSoto)
        {
            InitializeComponent();
            label2.Text = text1;
            //menu = true;
            if (menu == true)
            {
                label3.Show();
                radioButton1.Show();
                radioButton2.Show();
                if(isSoto == true)
                {
                    radioButton2.Text = "Pangsit";
                    text = "a";
                }
                else if(isSoto == false)
                {
                    radioButton2.Text = "Telor Ceplok";
                    text = "b";
                }
            }

            //numericUpDown1.Minimum = numericUpDown1.Value + 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                if(text == "a")
                {
                    bool menuTmbhn = false;
                    int count = Convert.ToInt32(numericUpDown1.Value);
                    int hrg = sa.Harga(count, menuTmbhn);
                    MessageBox.Show(hrg.ToString());
                }
                else if (text == "b")
                {
                    bool menuTmbhn = false;
                    int count = Convert.ToInt32(numericUpDown1.Value);
                    int hrg = ng.Harga(count, menuTmbhn);
                    MessageBox.Show(hrg.ToString());
                }
            }
            else if (radioButton2.Checked)
            {
                if(text == "a")
                {
                    bool menuTmbhn = true;
                    int count = Convert.ToInt32(numericUpDown1.Value);
                    int hrg = sa.Harga(count, menuTmbhn);
                    MessageBox.Show(hrg.ToString());
                }
                else if(text == "b")
                {
                    bool menuTmbhn = true;
                    int count = Convert.ToInt32(numericUpDown1.Value);
                    int hrg = ng.Harga(count, menuTmbhn);
                    MessageBox.Show(hrg.ToString());
                }
            }
            else
            {
                int count = Convert.ToInt32(numericUpDown1.Value);
                int hrg = ag.Harga(count);
                MessageBox.Show(hrg.ToString());
            }
        }
    }
}
